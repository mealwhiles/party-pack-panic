About
-----
These are the assets and project files for the 'Party Pack Panic' OBS scene collection.  
(YouTube playlist URL here)

Dependencies
------------
Open the Linux or Windows .json file in OBS.  
https://obsproject.com

The StreamFX plugin is needed for the more updated Windows file.  
https://obsproject.com/forum/resources/streamfx-for-obs-studio.578/

Contact
-------
mealwhiles@mealwhiles.com

PGP Public Key  
https://mealwhiles.com/pubkey
E98F31A59E315126C46E3A6C6B7126B9690CF3A5  
Tutanota emails will be encrypted properly.
