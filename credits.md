Party Pack Panic Assets & Scene Collection  
https://gitlab.com/mealwhiles/party-pack-panic

Assets that are not credited are made by me.  
If you find a mistake, please contact me!

Contact
--------
mealwhiles@mealwhiles.com

PGP Public Key  
https://keyserver.pgp.com/vkd/DownloadKey.event?keyid=0x6B7126B9690CF3A5  
E98F31A59E315126C46E3A6C6B7126B9690CF3A5  
Tutanota emails will be encrypted properly.

Graphics
--------

[0] "start" scene  
"Purism Phone"  
file phone.png  
by Purism  
https://puri.sm

Other
-----

<0>  
"Liberation Sans"  
typeface asset  
by Steve Matteson  
licensed by Red Hat

<1>  
"Cimero Pro"  
typeface asset  
by Neogrey Creative

Freedom-respecting Software
---------------------------

"Open Broadcaster Software"  
for recording  
https://obsproject.com

"GNU Image Manipulation Program"  
for image manipulation  
https://gimp.org

"Kdenlive"  
for video editing  
https://kdenlive.org

"Natron" 
for video compositing  
https://natrongithub.github.io

Proprietary Software
--------------------

Elgato Game Capture HD  
for capture card drivers  
in win-prime-time.json  
https://elgato.com
